package im.gitter.gitter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

import im.gitter.gitter.network.ApiRequest;
import im.gitter.gitter.network.VolleySingleton;

public class NotificationsDialog {

    private List<String> apiOptions;
    private CharSequence[] dialogOptions;
    private final RequestQueue requestQueue;
    private Context context;
    private String uri;

    public NotificationsDialog(Context context, String roomId) {
        this.context = context;
        this.requestQueue = VolleySingleton.getInstance(context).getRequestQueue();
        this.uri = "/v1/user/me/rooms/" + roomId + "/settings/notifications";

        apiOptions = Arrays.asList("all", "announcement", "mute");
        dialogOptions = new CharSequence[] {
                createOption(R.string.notification_settings_all, R.string.notification_settings_all_description),
                createOption(R.string.notification_settings_announcement, R.string.notification_settings_announcement_description),
                createOption(R.string.notification_settings_mute, R.string.notification_settings_mute_description)
        };
    }

    public void show() {
        requestQueue.add(new ApiRequest<String>(context, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String mode) {
                int selectedOption = apiOptions.indexOf(mode);
                showDialog(selectedOption);
            }
        }, null) {
            @Override
            protected String parseJsonInBackground(String json) throws JSONException {
                return new JSONObject(json).getString("mode");
            }
        }.setTag(context));
    }

    private CharSequence createOption(int titleResource, int descriptionResource) {
        Resources res = context.getResources();
        CharSequence title = res.getString(titleResource);
        SpannableString description = new SpannableString(res.getString(descriptionResource));
        description.setSpan(new RelativeSizeSpan(0.75f), 0, description.length(), 0);
        description.setSpan(new ForegroundColorSpan(Color.GRAY), 0, description.length(), 0);
        return TextUtils.concat(title, "\n", description);
    }

    private void showDialog(int selected) {
        AlertDialog dialog = new AlertDialog.Builder(context)
                .setTitle(R.string.notification_settings_option)
                .setSingleChoiceItems(dialogOptions, selected, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // do nothing
                    }
                })
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ListView singleChoiceView = ((AlertDialog) dialog).getListView();
                        int option = singleChoiceView.getCheckedItemPosition();
                        submit(option);
                    }
                }).create();

        View footer = LayoutInflater.from(context).inflate(R.layout.notifications_dialog_footer, null);
        dialog.getListView().addFooterView(footer);

        dialog.show();

        if (selected < 0) {
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
        }
    }

    private void submit(final int option) {
        String apiOption = apiOptions.get(option);
        try {
            JSONObject json = new JSONObject("{ mode: '" + apiOption + "'}");
            requestQueue.add(new ApiRequest<Void>(context, Request.Method.PUT, uri, json, new Response.Listener<Void>() {
                @Override
                public void onResponse(Void response) {}
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(
                            context,
                            R.string.notification_settings_failed,
                            Toast.LENGTH_SHORT
                    ).show();
                }
            }) {
                @Override
                protected Void parseJsonInBackground(String json) throws JSONException {
                    return null;
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}